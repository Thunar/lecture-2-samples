

module Y; end

class A; end

class B < A
  include Y
end

B.superclass
=> A
A.superclass
=> Object
Object.superclass
=> BasicObject
BasicObject.superclass
=> nil

B.ancestors
=> [B, Y, A, Object, Kernel, BasicObject]