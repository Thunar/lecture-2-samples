


# inheritance polymorphism
class Report
  def initialize; end

  def build
    raise NotImplementedError, 'You must implement the build method'
  end
end

class HtmlReport < Document
  def build
    p 'Print from HtmlReport'
  end
end

class PdfReport < Document
  def build
    p 'Print from PdfReport'
  end
end

# polymorphism on different objects
class ReportHandler
  def initialize(options = {})
    @report_builder = options.fetch(:report_builder)
  end

  def build_report
    raise ReportBuildFailed unless report_builder.respond_to?(:build)
    report_builder.build
  end
end

class HistoricalPdfReport
  def build
    p 'Print from HistoricalPdfReport'
  end
end


