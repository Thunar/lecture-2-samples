

module Shouting
  refine String do
    def shout
      self.upcase + "!"
    end
  end
end

class Thing
  using Shouting

  "hello".shout # => "HELLO!"
end