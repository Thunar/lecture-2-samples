
Class.class
=> Class
Module.class
=> Class

Class.superclass
=> Module
Module.superclass
=> Object

Class.instance_methods - Module.instance_methods
=> [:new, :allocate, :superclass]

Class.ancestors
=> [Class, Module, Object, Kernel, BasicObject]
Module.ancestors
=> [Module, Object, Kernel, BasicObject]

